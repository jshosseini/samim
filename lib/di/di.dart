
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:injectable/injectable.dart';
import '../data/common/http_client.dart';
import 'di.config.dart';


final getIt=GetIt.instance;
@InjectableInit(
  initializerName: 'init', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
)
void configureDependencies() {
  getIt.registerSingleton<Dio>(httpClient);
  init(getIt);
}
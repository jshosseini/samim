
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../common/exception.dart';
import '../../domain/CountryUseCase.dart';
import '../../domain/modle/Country.dart';

part 'country_event.dart';
part 'country_state.dart';

class CountryBloc extends Bloc<CountryEvent, CountryState> {

  final ICountryUseCase useCase;
  CountryBloc(this.useCase) : super(CountryLoadingState()) {
    on<CountryEvent>((event, emit)async {
      try {
        if (event is CountryStarted) {
          emit(CountryLoadingState());
          final reslut= await useCase.getCountries();
          if(reslut.isNotEmpty)
            {
              emit(CountrySuccessState(reslut));
            }
          else {
            emit(CountryEmptyState("لیست کشورها خالی میباشد."));
          }

        }
      }catch(e){
        emit(CountryErrorState(AppException(message: "خطای ناشناخته")));
      }
    });
  }
}

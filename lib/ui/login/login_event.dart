part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
  List<Object?> get props =>[];
}
class LoginStarted extends LoginEvent {
  const LoginStarted();
}
class LoginBtnClicked extends LoginEvent{

  final String userName;
  final String passWord;
  const LoginBtnClicked(this.userName, this.passWord);
}

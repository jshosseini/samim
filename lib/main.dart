import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:samim/common/theme.dart';
import 'package:samim/ui/country/country.dart';
import 'package:samim/ui/login/Login.dart';
import 'common/routes.dart';
import 'di/di.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  runApp(const MyApp());
}


class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ThemeMode themeMode = ThemeMode.dark;
  Locale myLocale = const Locale("en");

  @override
  Widget build(BuildContext context) {

    return GetMaterialApp(
      //support MultiLanguage
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', '1'), // English, no country code
        Locale('fa', '98'), // Spanish, no country code
      ],
      locale:myLocale,
      theme: themeMode == ThemeMode.dark ? ThemDataConfig.darck().getThemeData(myLocale.languageCode)
                                         : ThemDataConfig.light().getThemeData(myLocale.languageCode),
      //navigation
      initialRoute:Routes.defultScreen,
      getPages: [
        GetPage(name:Routes.defultScreen, page: () =>  const LoginScreen()),
        GetPage(name:Routes.loginScreen, page: () => const LoginScreen(),transitionDuration: const Duration(milliseconds: 500),transition: Transition.leftToRight ),
        GetPage(name:Routes.countryScreen, page: () => CountryScreen(togleTheme: () => setState(() {
            if (themeMode == ThemeMode.dark) {
              themeMode = ThemeMode.light;
            } else {
              themeMode = ThemeMode.dark;
            }
          }), slectedLanguageChanged:(language newSelectedLanguageByUser) {
    setState(() {
    myLocale = newSelectedLanguageByUser == language.en
    ? Locale('en')
        : Locale('fa');
    Get.updateLocale(myLocale);
    }
    );
    }),transitionDuration: const Duration(milliseconds: 500),transition: Transition.leftToRightWithFade)
      ],
      title: 'Samim Application',
      debugShowCheckedModeBanner: false,
    );
  }
}
import 'package:injectable/injectable.dart';

import '../dataSource/loginDataSorce.dart';

abstract class ILoginRepository implements ILoginDataSource {

}

@LazySingleton(as: ILoginRepository)
class LoginRepository implements ILoginRepository{
  final ILoginDataSource loginDataSource;
  LoginRepository(this.loginDataSource);

  @override
  Future<bool> checkUserPassword(String userName, String passwrod) {
    return loginDataSource.checkUserPassword(userName, passwrod);
  }

}
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';




abstract class ICountryDataSource{
 Future<List<dynamic>> getCountries();
}
@LazySingleton(as:ICountryDataSource)
class CountryDataSource implements ICountryDataSource{
  final Dio httpClient;
  CountryDataSource(this.httpClient);

  @override
  Future<List<dynamic>> getCountries() async {
    final response=await httpClient.get('countriesV2.json');
    return  json.decode(response.data);
    //return (response.data as List).map((item) => ClassEntity.formJson(item)).toList();
  }
}
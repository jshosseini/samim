import 'package:dio/dio.dart';

final httpClient =
    Dio(BaseOptions(baseUrl: 'https://raw.githubusercontent.com/esmaeil-ahmadipour/restcountries/main/json/'))
      ..interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
        handler.next(options);
      }
      )
      );


import 'package:injectable/injectable.dart';
import '../data/repository/countryRepository.dart';
import 'modle/Country.dart';

abstract class ICountryUseCase {
  Future<List<Country>> getCountries();
}
@LazySingleton(as: ICountryUseCase)
class CountryUseCase implements ICountryUseCase{
  final ICountryRepository repository;
  CountryUseCase(this.repository);

  @override
  Future<List<Country>> getCountries()async {
    final result=await repository.getCountries();
    return result.map((country) => Country.fromJson(country)).toList();
  }


}
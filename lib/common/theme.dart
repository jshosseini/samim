import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemDataConfig {
  static const String fontFamilyFa = "IranYekan";
  final Color primaryColor = Colors.pink.shade400;
  final Color primaryTextColor;
  final Color secondaryTextColor;
  final Color surfaceColor;
  final Color backgroundColor;
  final Color appBarColor;
  final Brightness brightness;
  ThemDataConfig.darck()
      : primaryTextColor = Colors.white,
        secondaryTextColor = Colors.white70,
        surfaceColor = const Color(0x0dffffff),
        backgroundColor = const Color.fromARGB(255, 30, 30, 30),
        appBarColor = Colors.black,
        brightness = Brightness.dark;
  ThemDataConfig.light()
      : primaryTextColor = Colors.grey.shade900,
        secondaryTextColor = Colors.grey.shade900.withOpacity(0.8),
        surfaceColor = const Color(0x0d000000),
        appBarColor = const Color.fromARGB(255, 235, 235, 235),
        backgroundColor = Colors.white,
        brightness = Brightness.light;
  ThemeData getThemeData(String languageCode) {
    return ThemeData(
        primarySwatch: Colors.pink,
        primaryColor: primaryColor,
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(primaryColor))),
        dividerColor: surfaceColor,
        dividerTheme: const DividerThemeData(),
        brightness: brightness,
        snackBarTheme:  SnackBarThemeData(contentTextStyle:TextStyle(fontWeight: FontWeight.w900, color: secondaryTextColor)),
        inputDecorationTheme: InputDecorationTheme(
            labelStyle: const TextStyle(
                fontSize: 14, fontWeight: FontWeight.normal),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide.none),
            filled: true),
        scaffoldBackgroundColor: backgroundColor,
        appBarTheme: AppBarTheme(
            backgroundColor: appBarColor,
            foregroundColor: primaryTextColor,
            elevation: 0),
        textTheme: languageCode == "fa" ? faTextTheme : enTextTheme);
  }

    TextTheme get enTextTheme => GoogleFonts.latoTextTheme(TextTheme(
        bodyText1: TextStyle(fontSize: 15, color: primaryTextColor),
        bodyText2: TextStyle(fontSize: 13, color: secondaryTextColor),
        subtitle1: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: primaryTextColor),
        headline6: TextStyle(fontWeight: FontWeight.w900, color: secondaryTextColor)));

    TextTheme get faTextTheme => TextTheme(
        bodyText1: TextStyle(
            fontSize: 15, color: primaryTextColor, fontFamily: fontFamilyFa),
        bodyText2: TextStyle(
            fontSize: 13,
            height: 1.5,
            color: secondaryTextColor,
            fontFamily: fontFamilyFa),
        subtitle1: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: primaryTextColor,
            fontFamily: fontFamilyFa),
        caption: TextStyle(fontFamily: fontFamilyFa),
        button: TextStyle(fontFamily: fontFamilyFa),
        headline6:
        TextStyle(fontWeight: FontWeight.w900, color: secondaryTextColor));


}

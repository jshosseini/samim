import 'package:flutter/cupertino.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


enum DisplaySize{
  extraSmall,//mobile
  small,//tablet
  medium,//laptop
  large//desktop
}

 extension DisplaySizeDetector on MediaQueryData{

   DisplaySize get displaySize {
     if (size.width < 600) {
       return DisplaySize.extraSmall;
     }
     if (size.width < 1248) {
       return DisplaySize.small;
     }
     if (size.width < 1448) {
       return DisplaySize.medium;
     }
     else {
       return DisplaySize.large;
     }
   }


   String get displayName {
     switch (displaySize) {
       case DisplaySize.extraSmall:
         return 'mobile';
       case DisplaySize.small:
         return 'tablet';
       case DisplaySize.medium:
         return 'laptop';
       case DisplaySize.large:
         return 'desktop';
     }
   }
 }
